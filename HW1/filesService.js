// requires...
const fs = require('fs');
const path = require('path');

// constants...
const DEFAULT_PATH = path.join(__dirname, 'files');

const createFile = (req, res, next) => {
    const {filename: fileName, content: fileContent} = req.body;
    if (!fileContent) {
        res.status(400).send({
            "message": "Please specify 'content' parameter"
        });
    } else {
        fs.writeFileSync(path.join(DEFAULT_PATH, fileName), fileContent);
        res.status(200).send({
            "message": "File created successfully"
        });
    }
}

const getFiles = (req, res, next) => {
    try {
        res.status(200).send({
            "message": "Success",
            "files": fs.readdirSync(DEFAULT_PATH)
        });
    } catch (e) {
        res.status(400).send({
            "message": "Client error"
        });
    }
}

const getFile = (req, res, next) => {
    const fileNames = fs.readdirSync(DEFAULT_PATH);
    const fileName = req.params.filename;
    const filePath = path.join(DEFAULT_PATH, fileName);
    if (fileNames.includes(fileName)) {
        res.status(200).send({
            "message": "Success",
            "filename": path.basename(filePath),
            "content": fs.readFileSync(filePath).toString(),
            "extension": path.extname(filePath).slice(1),
            "uploadedDate": fs.statSync(filePath).mtime
        });
    } else {
        res.status(400).send({
            "message": `No file with '${fileName}' filename found`
        });
    }
}

const changeFile = (req, res, next) => {
    const fileNames = fs.readdirSync(DEFAULT_PATH);
    const {filename: fileName, content: fileContent} = req.body;
    if (fileNames.includes(fileName)) {
        fs.writeFileSync(path.join(DEFAULT_PATH, fileName), fileContent);
        res.status(200).send({
            "message": "File successfully modified"
        });
    } else {
        res.status(400).send({
            "message": `No file with '${fileName}' filename found`
        });
    }
}

const deleteFile = (req, res, next) => {
    const fileNames = fs.readdirSync(DEFAULT_PATH);
    const fileName = req.params.filename;
    const filePath = path.join(DEFAULT_PATH, fileName);
    if (fileNames.includes(fileName)) {
        fs.unlinkSync(filePath);
        res.status(200).send({
            "message": "File successfully deleted"
        });
    } else {
        res.status(400).send({
            "message": `No file with '${fileName}' filename found`
        });
    }
}


module.exports = {
    createFile,
    getFiles,
    getFile,
    changeFile,
    deleteFile
}