const express = require('express');
const router = express.Router();

const { createFile, getFiles, getFile, changeFile, deleteFile } = require('./filesService.js');

router.post('/', createFile);

router.get('/', getFiles);

router.get('/:filename', getFile);

router.put('/', changeFile);

router.delete('/:filename', deleteFile);


module.exports = {
    filesRouter: router
};
