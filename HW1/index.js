const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();

const { filesRouter } = require('./filesRouter.js');

const errorHandler = (err, req, res, next) => {
    console.error(err);
    res.status(500).send({"message": "Server error"});
}

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/files', filesRouter);

const start = () => {
    try {
        if (!fs.existsSync('files')) {
            fs.mkdirSync('files');
        }
        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();
//ERROR HANDLER
app.use(errorHandler);